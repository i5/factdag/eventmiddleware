# EventMiddleware
An Apache Camel application that receives Activity Stream messages from an ActiveMQ queue, 
processes the messages to expose the respective resource they were generated for and writes 
the messages back to  an ActiveMQ topic for client consumption.

## Configuration 
The middleware has the following default configuration: 

`BROKER_HOST: localhost`

`BROKER_PORT: 61616`

`BROKER_INPUT_NAME: trellis`

`BROKER_INPUT_TYPE: queue`

`BROKER_OUTPUT_NAME: output`

`BROKER_OUTPUT_TYPE: topic`

To change the configuration, the environment variables can be overwritten.

## Docker
The Middleware can be built as a docker image using
`docker build -t eventmiddleware:latest .`

Using `docker-compose up -d`, the Middleware can be deployed to a docker environment. 
The deployment can be configured by editing the [docker-compose.yml](docker-compose.yml) file. 

More likely, the middleware is deployed as a part of a larger deployment together with a MessageBroker.
An example can be found in the [factlib.js-Project](https://git.rwth-aachen.de/i5/factdag/factlibjs)
