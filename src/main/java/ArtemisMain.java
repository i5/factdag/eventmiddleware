import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.main.Main;

public final class ArtemisMain {
    private static Main main = new Main();
    private ArtemisMain() {}

    public static void main(String[] args) throws Exception {
        main.bind("artemis", createArtemisComponent());
        main.addRouteBuilder(new ActivityStreamRoute());
        main.run();
    }


    private static JmsComponent createArtemisComponent() {
        String host = System.getenv().getOrDefault("BROKER_HOST", "localhost");
        int port = Integer.parseInt(System.getenv().getOrDefault("BROKER_PORT", "61616"));
        String brokerURL =  "tcp://" + host + ":" + port;
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerURL);
        JmsConfiguration configuration = new JmsConfiguration();
        configuration.setConnectionFactory(connectionFactory);
        return new JmsComponent(configuration);
    }

}