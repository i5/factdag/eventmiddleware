import org.apache.camel.Endpoint;
import org.apache.camel.builder.RouteBuilder;
import org.trellisldp.camel.ActivityStreamProcessor;

import java.util.Properties;

import static org.apache.camel.Exchange.*;
import static org.apache.camel.builder.PredicateBuilder.and;
import static org.apache.camel.builder.PredicateBuilder.not;
import static org.apache.camel.component.http4.HttpMethods.POST;
import static org.apache.camel.model.dataformat.JsonLibrary.Jackson;
import static org.trellisldp.camel.ActivityStreamProcessor.ACTIVITY_STREAM_OBJECT_ID;
import static org.trellisldp.camel.ActivityStreamProcessor.ACTIVITY_STREAM_TYPE;


public class ActivityStreamRoute extends RouteBuilder {

    public ActivityStreamRoute() {
    }

    @Override
    public void configure() {
        String inputName = System.getenv().getOrDefault("BROKER_INPUT_NAME", "trellis");
        String inputType = System.getenv().getOrDefault("BROKER_INPUT_TYPE", "queue");
        String outputName = System.getenv().getOrDefault("BROKER_OUTPUT_NAME", "output");
        String outputType = System.getenv().getOrDefault("BROKER_OUTPUT_TYPE", "topic");

        Endpoint newActivity = endpoint("artemis:" + inputType + ":" + inputName);
        Endpoint resources = endpoint("artemis:" +outputType + ":"+ outputName);

        from(newActivity)
                .unmarshal()
                .json(Jackson)
                .process(new ActivityStreamProcessor())
                .filter(and(
                        not(header(ACTIVITY_STREAM_TYPE).contains("Delete")),
                        header(ACTIVITY_STREAM_OBJECT_ID).isNotNull()))
                .setHeader(HTTP_METHOD).constant(POST)
                .setHeader(CONTENT_TYPE).constant("application/x-www-form-urlencoded")
                .transform().simple("")
                .to("log:resources")
                .to(resources);
    }
}